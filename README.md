# Objectifs
> 0.0.1 : Poser les idées en vracs

## Apprendre Rust

- Faire un appel Http
- Monter un serveur de queuing
    - et notification via webSocket  

- Utiliser des webSocket  

- Créer :
    - un controleur d'API
    - un parseur
    - une lib
    - un webcomponent
    - un site web
    - un service  

- Matriser : 
    - la gestion des erreurs
    - Asynchrone
    - Appel BDD (postgresql, couchDB, influxDB)  


- Trouver une API ouverte pour avoir du concret :
    - https://api.gouv.fr/les-api/api_hubeau_piezometrie  
    - https://api.gouv.fr/les-api/api_hubeau_qualite_nappes_eau_sout  

- Target :
  - Web Composant
  - Notification sur le bureau
  - Carte Géo avec les différents points

...